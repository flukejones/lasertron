use std::{collections::VecDeque, time::Instant};

use rustfft::num_complex::Complex32;

use crate::channels::Channels;

#[derive(Debug, Default, PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
pub enum Smoothing {
    None,
    #[default]
    MonsterCat,
    Sgs,
}

#[derive(Debug, Default, Clone, Copy)]
pub struct SgsSmoothingConfig {
    pub passes: i32,
    pub points: i32,
}

#[derive(Debug, Default, Clone, Copy)]
pub struct MonstercatSmootingConfig {
    pub factor: f32,
}
#[derive(Debug, Clone, Copy)]
pub struct SpectrumConfig {
    pub falloff: f32,
    pub low_cutoff: u32,
    pub high_cutoff: u32,
    pub smoothing: Smoothing,
    pub sgs_smoothing: SgsSmoothingConfig,
    pub monstercat_smoothing: MonstercatSmootingConfig,
}

impl Default for SpectrumConfig {
    fn default() -> Self {
        Self {
            falloff: 0.91,
            low_cutoff: 30,
            high_cutoff: 22050,
            smoothing: Smoothing::Sgs,
            sgs_smoothing: SgsSmoothingConfig {
                passes: 1,
                points: 3,
            },
            monstercat_smoothing: MonstercatSmootingConfig { factor: 1.5 },
        }
    }
}

/// Analyser suitable for displaying bar style visuals
pub struct Spectrum {
    config: SpectrumConfig,
    freq_range: Vec<usize>,
    freq_peaks: Channels<f32>,
    /// This is the maximum value the frequencies reach, used to determine
    /// the percentage of range frequencies hit
    max_peak: f32,
    max_peaks: VecDeque<f32>,
    _last_time: Instant,
    monstercat_smoothing_weights: Vec<f32>,
}

impl Spectrum {
    pub fn new(freq_range: Vec<usize>, config: SpectrumConfig) -> Self {
        Spectrum {
            config,
            max_peaks: VecDeque::with_capacity(freq_range.len()),
            freq_peaks: Channels::new(freq_range.len()),
            freq_range,
            max_peak: 0.0,
            _last_time: Instant::now(),
            monstercat_smoothing_weights: Vec::new(),
        }
    }

    pub fn frequency_peaks(&self) -> &Channels<f32> {
        &self.freq_peaks
    }

    pub fn get_max_peak(&self) -> f32 {
        self.max_peak
    }

    pub fn reset_peak(&mut self) {
        self.max_peak = 0.0;
    }

    pub fn generate_bars(
        &mut self,
        bar_count: usize,
        fft: &Channels<Complex32>,
        sampling_rate: usize,
    ) {
        self.regenerate_freq_bins(bar_count);

        Self::generate_bar(
            fft.left(),
            self.freq_peaks.left_mut(),
            &self.freq_range,
            &mut self.max_peak,
            sampling_rate,
        );
        Self::generate_bar(
            fft.right(),
            self.freq_peaks.right_mut(),
            &self.freq_range,
            &mut self.max_peak,
            sampling_rate,
        );

        self.smooth(Smoothing::MonsterCat);

        self.erode_peaks();
    }

    fn generate_bar(
        fft_channel: &[Complex32],
        channel_peaks: &mut [f32],
        freq_range: &[usize],
        max_peak: &mut f32,
        sampling_rate: usize,
    ) {
        let freq_diviser = sampling_rate / fft_channel.len();
        for (count, frame) in fft_channel.iter().enumerate() {
            let magnitude = frame.norm().sqrt();
            let freq = count * freq_diviser;
            for (idx, f) in channel_peaks.iter_mut().enumerate() {
                if idx < freq_range.len() - 1
                    && freq >= freq_range[idx]
                    && freq < freq_range[idx + 1]
                    && magnitude > *f
                {
                    *f = magnitude; // / (self.freq_range[idx + 1] - self.freq_range[idx]) as f32;
                    *f *= (2.0 + idx as f32).log2() * (100.0 / freq_range.len() as f32);
                    *f = f.powf(0.5);
                    if *f > *max_peak {
                        *max_peak = *f;
                    }
                    break;
                }
            }
        }
    }

    /// Regenerate the frequency range bins (if the bar count changes)
    fn regenerate_freq_bins(&mut self, bar_count: usize) {
        if bar_count == self.freq_range.len() - 1 {
            return;
        }

        self.freq_range = vec![0; bar_count + 1];
        self.freq_peaks = Channels::new(self.freq_range.len());
        // TODO: put these in options
        let low_cutoff = self.config.low_cutoff as f32;
        let high_cutoff = self.config.high_cutoff as f32;

        let freq_const =
            (low_cutoff / high_cutoff).log10() / ((1.0 / (bar_count + 1) as f32) - 1.0);

        let mut freq_const_per_bin = vec![0.0; bar_count + 1];

        for (i, freq) in freq_const_per_bin.iter_mut().enumerate() {
            // The calculation of bar bins is >= low cutoff[i] && < low_cutoff[i+1]
            let tmp =
                (freq_const * -1.0) + (((i as f32 + 1.0) / (bar_count as f32 + 1.0)) * freq_const);
            *freq = high_cutoff * 10.0f32.powf(tmp);

            // TODO: get sampling frequency
            let frequency = *freq / 44800.0 / 2.0;
            // TODO: sample_size should be sampling freq / fps
            //  currently FPS is unbounded in the sdl2 example
            let sampling_freq = 44800.0 / 1.0;
            self.freq_range[i] = (frequency * sampling_freq) as usize;
        }
    }

    fn smooth(&mut self, smoothing: Smoothing) {
        match smoothing {
            Smoothing::None => {}
            Smoothing::MonsterCat => {
                let factor = self.config.monstercat_smoothing.factor;
                let smoothing = &mut self.monstercat_smoothing_weights;
                let left = self.freq_peaks.left_mut();
                Self::smooth_monstercat(left, factor, smoothing);
                let right = self.freq_peaks.right_mut();
                Self::smooth_monstercat(right, factor, smoothing);
            }
            Smoothing::Sgs => {
                let points = self.config.sgs_smoothing.points;
                let passes = self.config.sgs_smoothing.passes;
                let left = self.freq_peaks.left_mut();
                Self::smooth_sgs(points, passes, left);
                let right = self.freq_peaks.right_mut();
                Self::smooth_sgs(points, passes, right);
            }
        }
    }

    fn smooth_sgs(passes: i32, points: i32, bars: &mut [f32]) {
        let mut orig = bars.to_vec();

        for pass in 0..passes {
            let pivot = (points as f32 / 2.0).floor() as usize;

            for pivot in 0..pivot {
                bars[pivot] = orig[pivot];
                bars[orig.len() - pivot - 1] = orig[orig.len() - pivot - 1]
            }

            let smooth_const = 1.0 / (2.0 * pivot as f32 + 1.0);

            for (i, bar) in bars.iter_mut().enumerate() {
                let mut sum = 0.0;
                for j in 0..=2 * pivot {
                    let mut index = i + j - pivot;
                    if index > orig.len() - 1 {
                        index = orig.len() - 1;
                    }
                    sum += (smooth_const * orig[index] as f32) + j as f32 - pivot as f32;
                }
                *bar = sum;
            }

            if pass < passes {
                orig = bars.to_vec();
            }
        }
    }

    fn smooth_monstercat(
        bars: &mut [f32],
        smoothing_factor: f32,
        smoothing_weights: &mut Vec<f32>,
    ) {
        if smoothing_weights.len() != bars.len() {
            *smoothing_weights = vec![0.0; bars.len()];
            for (i, weight) in smoothing_weights.iter_mut().enumerate() {
                *weight = smoothing_factor.powf(i as f32);
            }
        }

        for i in 1..bars.len() {
            if bars[i] < 0.1 {
                bars[i] = 0.1;
            } else {
                for j in 0..bars.len() {
                    if i != j {
                        let index = (i as i32 - j as i32).unsigned_abs() as usize;
                        let weighted_value = bars[i] / smoothing_weights[index];
                        if bars[j] < weighted_value {
                            bars[j] = weighted_value;
                        }
                    }
                }
            }
        }
    }

    /// We find the average to try and keep the peaks at a reasonably consistent max height
    pub fn average_peak(&mut self) {
        let peak = self.max_peak;
        if peak > self.max_peak {
            self.max_peaks.push_front(peak);
            if self.max_peaks.len() > self.freq_range.len() {
                self.max_peaks.pop_back();
            }

            let total = self.max_peaks.iter().fold(0.0, |acc, x| acc + x);
            self.max_peak = total / self.max_peaks.len() as f32 - 1.0;
        }
    }

    fn erode_peaks(&mut self) {
        // let fac = 0.005f32.powf(self.last_time.elapsed().as_secs_f32());
        // self.last_time = Instant::now();
        let fac = self.config.falloff;
        for n in self.freq_peaks.left_mut().iter_mut() {
            *n *= fac;
        }
        for n in self.freq_peaks.right_mut().iter_mut() {
            *n *= fac;
        }
    }
}
