use channels::Channels;
use rustfft::num_complex::Complex32;

pub mod transformers;

mod channels;

/// Various audio samplers that will read/monitor audio and give a final FFT result
/// to work with
pub mod fft_buffer;

pub mod audio_source;

pub trait Source {
    fn read_to_buf(&self, sampler: &mut impl Sampler32);
    // fn read_to_buf(&self, buf: &mut [u8]);

    fn get_sampling_rate(&self) -> u32;
}

pub trait Sampler32 {
    /// Return the buffer the audio source should read in to
    fn input_buf(&mut self) -> &mut [u8];

    /// Set single or dual channel
    fn stereo(&mut self, on: bool);

    /// Transform data in to an FFT plan and process it. This step is required before
    /// a vidualisation can be made
    fn process_fft(&mut self);

    fn channel(&self) -> &Channels<Complex32>;
}

pub const PIONEER_SG750: [usize; 10] = [32, 63, 125, 250, 500, 1000, 2000, 4000, 8000, 16000];

pub const PIONEER_SG9: [usize; 12] = [
    16, 32, 64, 125, 250, 500, 1000, 2000, 4000, 8000, 16000, 32000,
];

pub const PIONEER_SG90: [usize; 16] = [
    16,
    32,
    63,
    100,
    160,
    250,
    400,
    630,
    1000,
    1600,
    2500,
    4000,
    6300,
    10000,
    16000,
    44100 / 2,
];

pub const CUSTOM_EQ: [usize; 23] = [
    0, 35, 75, // 63+ Bass
    125, 175, 200, 250, 800, // 250+ Low-mids to 2000
    1200, 1600, 2000, 2500, // 2000+ High-mids to 4000
    3000, 3500, 4000, 4500, 5000, // 4000+ Pressence
    5500, 6000, 6300, // 6000+ Brilliance
    18900, 31500, 40000,
];

// Sampling size of pulse should be frequency / target fps
