use rustfft::{algorithm::Radix4, num_complex::Complex32, Fft, FftDirection};
use std::mem::size_of;

use crate::{channels::Channels, Sampler32};

/// `CHANS` = channel count
/// `S` = samples count
/// `SPC` = samples per channel: samples / channels / size_of<sample type>
pub struct FftBuffer {
    planner: Radix4<f32>,
    /// Raw audio monitoring buffer. Bytes need building in to proper types
    monitor_buf: Vec<u8>,
    source_read_len: usize,
    buf_start: usize,
    buf_end: usize,
    /// Buffer for FFT plan
    fft_buf: Channels<Complex32>,
    stereo: bool,
}

impl FftBuffer {
    #[allow(clippy::new_without_default)]
    pub fn new(channels: usize, source_read_len: usize, samples_per_channel: usize) -> Self {
        let sampling_len = channels * samples_per_channel * size_of::<f32>();
        FftBuffer {
            planner: Radix4::new(samples_per_channel, FftDirection::Forward),
            monitor_buf: vec![0u8; sampling_len],
            buf_start: 0,
            buf_end: source_read_len,
            source_read_len,
            fft_buf: Channels::new(samples_per_channel),
            stereo: true,
        }
    }
}

impl Sampler32 for FftBuffer {
    fn input_buf(&mut self) -> &mut [u8] {
        if self.buf_start + self.buf_end < self.monitor_buf.len() - 1 {
            self.buf_start += self.source_read_len;
            self.buf_end += self.source_read_len;
        } else {
            self.buf_start = 0;
            self.buf_end = self.source_read_len
        }
        &mut self.monitor_buf[self.buf_start..self.buf_end]
        // &mut self.monitor_buf
    }

    fn process_fft(&mut self) {
        let mut trans_buf: [u8; 4] = [0, 0, 0, 0];
        for (index, entry) in (0..self.monitor_buf.len()).step_by(8).enumerate() {
            trans_buf.copy_from_slice(&self.monitor_buf[entry..=entry + 3]);
            let left = f32::from_le_bytes(trans_buf);
            self.fft_buf.left_mut()[index].re = left;
            self.fft_buf.left_mut()[index].im = 0.0;

            if self.stereo {
                trans_buf.copy_from_slice(&self.monitor_buf[entry + 4..=entry + 7]);
                let right = f32::from_le_bytes(trans_buf);
                self.fft_buf.right_mut()[index].re = right;
                self.fft_buf.right_mut()[index].im = 0.0;
            }
        }
        self.planner.process(self.fft_buf.left_mut());
        self.planner.process(self.fft_buf.right_mut());
    }

    fn stereo(&mut self, on: bool) {
        self.stereo = on;
    }

    fn channel(&self) -> &Channels<Complex32> {
        &self.fft_buf
    }
}
