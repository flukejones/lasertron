use libpulse_binding as pulse;

use pulse::{
    context::{Context, FlagSet},
    def::Retval,
    mainloop::standard::{IterateResult, Mainloop},
    operation::{Operation, State},
    sample::Spec,
};
use std::cell::RefCell;
use std::rc::Rc;

pub struct PulseConnection {
    mainloop: Mainloop,
    context: Context,
}

impl PulseConnection {
    pub fn new() -> Self {
        let mainloop = Mainloop::new().expect("Failed to create mainloop");
        let context = pulse::context::Context::new(&mainloop, "Vis Startup").unwrap();
        PulseConnection { mainloop, context }
    }

    fn iterate_self(&mut self) {
        match self.mainloop.iterate(false) {
            IterateResult::Success(_) => {}
            IterateResult::Quit(_) | IterateResult::Err(_) => {
                panic!("iterate state was not success, quitting...");
            }
        }
    }

    fn iterate_op_to_completion<T: ?Sized>(&mut self, op: Operation<T>) {
        loop {
            self.iterate_self();
            match op.get_state() {
                State::Done => break,
                State::Running => {}
                State::Cancelled => panic!("Operation cancelled without an error"),
            }
        }
    }

    pub fn connect(&mut self) {
        self.context
            .connect(None, FlagSet::NOFLAGS, None)
            .expect("Failed to connect context");

        // Need to poll the server until we get a Ready
        loop {
            self.iterate_self();
            match self.context.get_state() {
                pulse::context::State::Ready => break,
                pulse::context::State::Failed | pulse::context::State::Terminated => {
                    panic!("context state failed/terminated, quitting...");
                }
                _ => {}
            }
        }
    }

    pub fn mainloop(&mut self) -> &mut Mainloop {
        &mut self.mainloop
    }

    pub fn get_spec(&mut self) -> Option<Spec> {
        let name = Rc::new(RefCell::new(None));
        let n = name.clone();
        let operation = self.context.introspect().get_server_info(move |info| {
            let spec = info.sample_spec;
            n.borrow_mut().replace(spec);
        });
        // Run the operation
        self.iterate_op_to_completion(operation);
        let spec = name.borrow();
        *spec
    }

    /// Find the default device used for sound output and grab its monitor name
    pub fn get_default_monitor(&mut self) -> String {
        let name = Rc::new(RefCell::new(String::new()));
        let n = name.clone();
        let operation = self.context.introspect().get_server_info(move |info| {
            n.borrow_mut()
                .push_str(info.default_sink_name.as_ref().unwrap());
            n.borrow_mut().push_str(".monitor");
        });

        // Run the operation
        self.iterate_op_to_completion(operation);
        return name.borrow().to_string();
    }

    pub fn quit(&mut self) {
        self.mainloop.quit(Retval(0));
    }
}
