use std::mem::size_of;

use libpulse_binding::{
    def::BufferAttr,
    sample::{Format, Spec},
    stream::Direction,
};
use libpulse_simple_binding::Simple;

use crate::{Sampler32, Source};

pub struct SourcePulse(Simple, Spec);

impl SourcePulse {
    #[allow(clippy::new_without_default)]
    /// If `samples_per_channel` is too high latency may be unacceptable. The `samples_per_channel`
    /// should also be the same as the read-to-buffer length
    pub fn new(channels: usize, source_read_len: usize, mut spec: Spec, monitor: &str) -> Self {
        let sampling_len = channels * source_read_len * size_of::<f32>();

        spec.format = Format::F32le;
        assert!(spec.is_valid());

        let audio = Simple::new(
            None,
            "Audio EQ Analyzer",
            Direction::Record,
            if monitor.is_empty() {
                None
            } else {
                Some(monitor)
            },
            "Music Monitor",
            &spec,
            None,
            Some(&BufferAttr {
                maxlength: sampling_len as u32,
                tlength: 0,
                prebuf: 0,
                minreq: 0,
                fragsize: sampling_len as u32,
            }),
        )
        .map_err(|err| err.to_string().unwrap())
        .unwrap();

        audio.flush().unwrap();
        SourcePulse(audio, spec)
    }
}

impl Source for SourcePulse {
    fn read_to_buf(&self, sampler: &mut impl Sampler32) {
        self.0.read(sampler.input_buf()).unwrap();
    }

    fn get_sampling_rate(&self) -> u32 {
        self.1.rate
    }
}
