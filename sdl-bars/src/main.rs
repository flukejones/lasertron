mod timestep;

use lasertron::{
    audio_source::{pulse_connection::PulseConnection, SourcePulse},
    fft_buffer::FftBuffer,
    transformers::spectrum::{
        MonstercatSmootingConfig, SgsSmoothingConfig, Smoothing, Spectrum, SpectrumConfig,
    },
    Sampler32, Source,
};
use sdl2::{
    event::{Event, WindowEvent},
    keyboard::Keycode,
    pixels,
    rect::Rect,
};

const SCREEN_WIDTH: u32 = 1200;
const SCREEN_HEIGHT: u32 = 768;

// TODO: Use a callback for pulse?
// TODO: Optimise putting data in to complex with callback?

/*
 * - Sampling buffer length = size_of<Type (eg, i32)> * samples per channel * channels
*/

fn main() -> Result<(), String> {
    let sdl_context = sdl2::init()?;
    let video_subsys = sdl_context.video()?;
    let window = video_subsys
        .window("Audio EQ Test", SCREEN_WIDTH, SCREEN_HEIGHT)
        .resizable()
        .position_centered()
        .opengl()
        .build()
        .map_err(|e| e.to_string())?;

    let mut canvas = window
        .into_canvas()
        .accelerated()
        .present_vsync()
        .build()
        .map_err(|e| e.to_string())?;

    canvas.set_draw_color(pixels::Color::RGB(0, 0, 0));
    canvas.clear();
    canvas.present();

    let bar_width: u32 = 25;
    // basic audio setup part
    let channels = 2;
    let samples_per_channel = 1024 * 8;
    let source_read_len = 1024 * 2;

    let mut pulse_conn = PulseConnection::new();
    pulse_conn.connect();
    let spec = pulse_conn.get_spec().unwrap();
    let monitor = pulse_conn.get_default_monitor();
    let source = SourcePulse::new(channels, source_read_len, spec, &monitor);
    // transform the samples
    let mut spectrum = FftBuffer::new(channels, source_read_len, samples_per_channel);

    let sampling_rate = source.get_sampling_rate() as usize;

    let config = SpectrumConfig {
        falloff: 0.91,
        low_cutoff: 26,
        high_cutoff: 22050,
        smoothing: Smoothing::Sgs,
        sgs_smoothing: SgsSmoothingConfig {
            passes: 2,
            points: 3,
        },
        monstercat_smoothing: MonstercatSmootingConfig { factor: 1.5 },
    };
    let mut analyser = Spectrum::new(lasertron::PIONEER_SG90.to_vec(), config);

    let mut timestep = timestep::TimeStep::new();
    let mut events = sdl_context.event_pump()?;

    'main: loop {
        for event in events.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => break 'main,
                Event::Window { win_event, .. } => {
                    if let WindowEvent::Resized(x, y) = win_event {
                        // canvas.window_mut().set_size(x as u32, y as u32).unwrap();
                        canvas.set_logical_size(x as u32, y as u32).unwrap();
                    }
                }
                _ => {}
            }
        }

        if let Some(_fps) = timestep.frame_rate() {
            println!("{:?}", _fps);
        }

        source.read_to_buf(&mut spectrum);
        spectrum.process_fft();

        timestep.run_this(|_| {
            canvas.set_draw_color(pixels::Color::RGB(0, 0, 0));
            canvas.clear();

            let (width, height) = canvas.output_size().unwrap();

            analyser.generate_bars(
                (width as usize - bar_width as usize) / 2 / bar_width as usize,
                spectrum.channel(),
                sampling_rate,
            );
            let height = height as f32;

            for (column, val) in analyser.frequency_peaks().left().iter().enumerate() {
                let colour = (255.0 - 50.0) * val / analyser.get_max_peak() + 50.0;
                let val = val / analyser.get_max_peak() * height;

                let color = pixels::Color::RGB(colour as u8, bar_width as u8, bar_width as u8);
                canvas.set_draw_color(color);
                let rect = Rect::new(
                    column as i32 * bar_width as i32,
                    height as i32 - val as i32,
                    bar_width,
                    val as u32,
                );
                let _ = canvas.fill_rect(rect);
            }

            for (column, val) in analyser.frequency_peaks().right().iter().enumerate() {
                let colour = (255.0 - 50.0) * val / analyser.get_max_peak() + 50.0;
                let val = val / analyser.get_max_peak() * height;

                let color = pixels::Color::RGB(colour as u8, colour as u8, bar_width as u8);
                canvas.set_draw_color(color);
                let rect = Rect::new(
                    width as i32 - (column as i32 + 1) * bar_width as i32,
                    height as i32 - val as i32,
                    bar_width,
                    val as u32,
                );
                let _ = canvas.fill_rect(rect);
            }

            analyser.average_peak();
            canvas.present();
        });
        // std::thread::sleep(Duration::from_millis(1));
    }
    pulse_conn.quit();
    Ok(())
}
